import java.util.Scanner;

public class User_Input {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		float zahl1 = 0.0f, zahl2 = 0.0f;
		int zahl3, zahl4;
		double zahlDouble;
		long zahlLong;
		byte zahlByte;
		short zahlShort;
		boolean wahrheitswert;
		char buchstabe;
		
		String name;
		//Float
		System.out.println("Das Programm addiert zwei Zahlen zusammen.");
		System.out.print("Bitte geben Sie die erste Zahl ein: ");
		zahl1 = myScanner.nextFloat();
		
		System.out.print("Bitte geben Sie die zweite Zahl ein: ");
		zahl2 = myScanner.nextFloat();
		
		float summe = (zahl1 + zahl2);
		System.out.println(summe);
		
		//String
		System.out.print("Bitte geben Sie einen Namen ein: ");
		name = myScanner.next();
		System.out.println(name);
		
		//Integer
		System.out.println("Das Programm addiert zwei Zahlen zusammen.");
		System.out.print("Bitte geben Sie die erste Zahl ein: ");
		zahl3 = myScanner.nextInt();
		System.out.print("Bitte geben Sie die zweite Zahl ein: ");
		zahl4 = myScanner.nextInt();
		
		int summe2 = (zahl3 + zahl4);
		System.out.println(summe2);
		
		//Double
		System.out.print("Bitte geben Sie eine Zahl an, diese wird als Double ausgegeben: ");
		zahlDouble = myScanner.nextDouble();
		System.out.println(zahlDouble);
		
		//Long
		System.out.print("Bitte geben Sie eine Zahl an, diese wird als Long ausgegeben: ");
		zahlLong = myScanner.nextLong();
		System.out.println(zahlLong);
		
		//Byte
		System.out.print("Bitte geben Sie eine Zahl an, diese wird als Byte ausgegeben: ");
		zahlByte = myScanner.nextByte();
		System.out.println(zahlByte);
		
		//Short
		System.out.print("Bitte geben Sie eine Zahl an, diese wird als Short ausgegeben: ");
		zahlShort = myScanner.nextShort();
		System.out.println(zahlShort);
		
		//Boolean
		System.out.print("Bitte geben Sie true oder false an: ");
		wahrheitswert = myScanner.nextBoolean();
		System.out.println(wahrheitswert);
		
		//Character
		System.out.print("Bitte geben Sie einen Buchstaben an, dieser wird als Character ausgegeben: ");
		buchstabe = myScanner.next().charAt(0);
		System.out.println(buchstabe);
		
		
		myScanner.close();
		
	}

}
