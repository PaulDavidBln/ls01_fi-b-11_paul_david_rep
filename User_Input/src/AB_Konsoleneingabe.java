import java.util.Scanner;
public class AB_Konsoleneingabe {

	public static void main(String[] args) {
		Scanner UserInput = new Scanner(System.in);
		
		System.out.print("Guten Tag. Bitte geben Sie ihren Namen ein: ");
		String name = UserInput.next();
		
		//Double weil einige User ihr Alter genauer eingeben könnten z.B. 16,5
		System.out.print("Bitte geben Sie ihr Alter ein: ");
		double age = UserInput.nextDouble();
		
		System.out.println("Ihr Name ist " + name + " und Sie sind " + age + " Jahre alt.");
		UserInput.close();
	}

}
