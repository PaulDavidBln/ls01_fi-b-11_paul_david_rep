import java.util.Scanner;
public class Rechner {

	public static void main(String[] args) {
		Scanner UserInput = new Scanner(System.in);
		
		//Addition
		System.out.print("Bitte geben Sie eine ganze Zahl ein: ");
		int add1 = UserInput.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		int add2 = UserInput.nextInt();
		
		int sumadd = add1 + add2;
		
		System.out.print("\nDas Ergebnis der Addition lautet: ");
		System.out.println(add1 + " + " + add2 + " + " + " = " + sumadd);
		
		
		//Subtraktion
		System.out.print("\n\nBitte geben Sie eine ganze Zahl ein: ");
		int sub1 = UserInput.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		int sub2 = UserInput.nextInt();
		
		int sumsub = sub1 - sub2;
		
		System.out.print("Das Ergebnis der Subtraktion lautet: ");
		System.out.print(sub1 + " - " + sub2 + " = " + sumsub);
		
		
		//Multiplikation
		System.out.print("\n\nBitte geben Sie eine ganze Zahl ein: ");
		int mul1 = UserInput.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		int mul2 = UserInput.nextInt();
		
		int summul = mul1 * mul2;
		
		System.out.print("Das Ergebnis der Multiplikation lautet: ");
		System.out.print(mul1 + " * " + mul2 + " = " + summul);
		
		
		//Division
		System.out.print("\n\nBitte geben Sie eine ganze Zahl ein: ");
		int div1 = UserInput.nextInt();
		
		System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: ");
		int div2 = UserInput.nextInt();
		
		int sumdiv = div1 / div2;
		
		System.out.print("Das Ergebnis der Division lautet: ");
		System.out.print(div1 + " : " + div2 + " = " + sumdiv);
		
		UserInput.close();

	}

}
