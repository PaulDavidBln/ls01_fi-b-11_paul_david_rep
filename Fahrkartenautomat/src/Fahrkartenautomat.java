import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {
		double anzahlTickets;
		double ticketPreis = 0;
		double summe = 0;
		int ticketart = 0;
		Scanner tastatur = new Scanner(System.in);
		
		System.out.println("Fahrkartenbestellvorgang:");
		System.out.println("=========================\n");
		while(ticketart != 9) {
			
			if(ticketart != 9) {					
				System.out.println("Wählen Sie Ihre Wunschfahrkarte für Berlin AB aus: ");
				System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
				System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
				System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
				System.out.println("  Bezahlen (9)\n");
				
			do {
				System.out.print("Ihre Wahl: ");
				ticketart = tastatur.nextInt();
				if(ticketart != 1 && ticketart != 2 && ticketart != 3 && ticketart != 9)
					System.out.println(">>falsche Eingabe<<");
			}while(ticketart != 1 && ticketart != 2 && ticketart != 3 && ticketart != 9);
			
			if(ticketart == 1) {
				ticketPreis = 2.9;
			}else if(ticketart == 2){
				ticketPreis = 8.6;
			}else if(ticketart == 3) {
				ticketPreis = 23.5;
			}
			
			if(ticketart == 1 || ticketart == 2 || ticketart == 3) {
				do {
					System.out.print("Anzahl der Tickets: ");
					anzahlTickets = tastatur.nextDouble();
					if(anzahlTickets > 10 || anzahlTickets <= 0) {
						System.out.println(" >> Wählen Sie bitte eine Anzahl von 1 bis 10 Tickets aus.");
					}	
				}while(anzahlTickets > 10 || anzahlTickets <= 0);
				summe = runden(summe + (anzahlTickets * ticketPreis));
				System.out.println("\nZwischensumme: " + runden(summe) + "0€\n");
			}
		
		}
	}

		return summe;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);

		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.format("Noch zu zahlen: %4.2f € %n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfenemuenze;
		}
		return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			warte(255);
		}
		System.out.println("\n\n");
	}

	public static void warte(int milisekunde) {
		try {
			Thread.sleep(milisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der Rückgabebetrag in Höhe von %4.2f Euro %n", rueckgabebetrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				muenzeAusgeben(2, "EURO");
				rueckgabebetrag = runden(rueckgabebetrag -= 2.0);
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				muenzeAusgeben(1, "EURO");
				rueckgabebetrag = runden(rueckgabebetrag -= 1.0);
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				muenzeAusgeben(50, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.5);
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
			{
				muenzeAusgeben(20, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.2);
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-MÃ¼zen
			{
				muenzeAusgeben(10, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.1);
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
			{
				muenzeAusgeben(5, "CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.05);
			}
		}
	}

	public static double runden(double zahl) {
		zahl = Math.round(zahl * 100)/100.00;
		//System.out.println("Extra: " + zahl);
		return zahl;
	
    }

	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println("" + betrag + " " + einheit);
	}

	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double rueckgabebetrag;
		boolean defekt = false;

		while(defekt == false) {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n\n");
		}

	}
}