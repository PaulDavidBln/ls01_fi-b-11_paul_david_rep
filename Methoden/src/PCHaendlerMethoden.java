import java.util.Scanner;
public class PCHaendlerMethoden {
	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		String artikel = liesString(myScanner, "Was möchten Sie bestellen?");
		int anzahl = liesInt(myScanner, "Geben Sie die Anzahl ein: ");
		double preis = liesDouble(myScanner, "Geben Sie den Nettopreis ein: ");
		double mwst = liesDouble(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein: ");
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
		
		rechnungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);

		myScanner.close();
	}
	
	public static String liesString(Scanner mS, String text) {
		System.out.println(text);
		String artikel = mS.next();
		return artikel;
	}
	
	public static int liesInt(Scanner mS, String text) {
		System.out.print(text);
		int anzahl = mS.nextInt();
		return anzahl;
	}
	
	public static double liesDouble(Scanner mS, String text) {
		System.out.print(text);
		double wert = mS.nextDouble();
		return wert;
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		double nettogesamtpreis = anzahl * preis;
		return nettogesamtpreis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
	
	public static void rechnungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		System.out.println("Rechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

}
