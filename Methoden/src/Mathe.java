import java.util.Scanner;
public class Mathe{

    public static void main(String[] args){
        Scanner mS = new Scanner(System.in);
        double x = getx(mS, "Bitte geben Sie die Zahl ein, die quadriert werden soll: ");
        double quadrat = quadrierung(x);
        ausgabe(x, quadrat);

        mS.close();
    }

    public static double getx(Scanner mS, String text){
        System.out.print(text);
        double x = mS.nextDouble();
        return x;
    }

    public static double quadrierung(double x){
        double quadrat = x * x;
        return quadrat;
    }

    public static void ausgabe(double x, double quadrat){
        System.out.print("Das Quadrat von " + x + " lautet " + quadrat);
    }
}