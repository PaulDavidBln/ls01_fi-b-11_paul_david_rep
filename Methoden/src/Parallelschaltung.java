import java.util.Scanner;

public class Parallelschaltung {

	public static void main(String[] args) {
		Scanner mS = new Scanner(System.in);
		double r1 = getr(mS, "Bitte geben Sie den ersten Wert der Parallelschaltung ein: ");
		double r2 = getr(mS, "Bitte geben Sie den zweiten Wert der Parallelschaltung ein: ");
		double ersatzwiderstand = parallelschaltung(r1, r2);
		ausgabe(ersatzwiderstand);
		
		mS.close();
	}
	
	public static double getr(Scanner mS, String text) {
		System.out.print(text);
		double r = mS.nextDouble();
		return r;
	}
	
	public static double parallelschaltung(double r1, double r2) {
		double ersatzwiderstand = (r1 * r2)/ (r1 + r2);
		return ersatzwiderstand;
	}
	
	public static void ausgabe(double ersatzwiderstand) {
		System.out.println("Der Ersatzwiderstand der Parallelschaltung beträgt: " + ersatzwiderstand);
	}

}
