import java.util.Scanner;
public class Reihenschaltung {

	public static void main(String[] args) {
		Scanner mS = new Scanner(System.in);
		double r1 = getr(mS, "Bitte geben Sie den ersten Wert der Reihenschaltung ein: ");
		double r2 = getr(mS, "Bitte geben Sie den zweiten Wert der Reihenschaltung ein: ");
		double ersatzwiderstand = reihenschaltung(r1, r2);
		ausgabe(ersatzwiderstand);
		
		mS.close();
		
	}
	
	public static double getr(Scanner mS, String text) {
		System.out.print(text);
		double r = mS.nextDouble();
		return r;
	}
	
	public static double reihenschaltung(double r1, double r2) {
		double ersatzwiderstand = r1 + r2;
		return ersatzwiderstand;
	}
	
	public static void ausgabe(double ersatzwiderstand) {
		System.out.println("Der Ersatzwiderstand beträgt: " + ersatzwiderstand);
	}

}
