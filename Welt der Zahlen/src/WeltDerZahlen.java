/**
 *   Aufgabe:  Recherechieren Sie im Internet !
 *
 *   Sie dürfen nicht die Namen der Variablen verändern !!!
 *
 *   Vergessen Sie nicht den richtigen Datentyp !!
 *
 *
 * @version 1.0 from 21.08.2019
 * @author Paul David
 */

public class WeltDerZahlen {

    public static void main(String[] args) {

    /*  *********************************************************

         Zuerst werden die Variablen mit den Werten festgelegt!

    *********************************************************** */
        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        int anzahlPlaneten = 8 ;

        // Anzahl der Sterne in unserer Milchstraße
        int sterne = 100;

                // Wie viele Einwohner hat Berlin?
                int bewohnerBerlin = 3654000;

        // Wie alt bist du?  Wie viele Tage sind das?

        int alterTage = 5840;

                // Wie viel wiegt das schwerste Tier der Welt?
                // Schreiben Sie das Gewicht in Kilogramm auf!
                int gewichtKilogramm = 190000;

                        // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
                        int flaecheGroessteLand = 17130000;

                                // Wie groß ist das kleinste Land der Erde?

                                double flaecheKleinsteLand = 0.44;




    /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

    *********************************************************** */

        System.out.println("Anzahl der Planeten: " + anzahlPlaneten);
        System.out.println("Anzahl der Sterne: " + sterne + " Milliarden");
        System.out.println("Einwohner Berlin: " + bewohnerBerlin);
        System.out.println("Ich bin " + alterTage + " Tage alt.");
        System.out.println("Das schwerste Tier wiegt " + gewichtKilogramm + " Kilogramm.");
        System.out.println("Das größte Land der Erde hat " + flaecheGroessteLand + " Quadratkilometer");
        System.out.println("Das kleinste Land der Erde hat " + flaecheKleinsteLand + " Quadratkilometer");

        System.out.println(" *******  Ende des Programms  ******* ");

    }
}
