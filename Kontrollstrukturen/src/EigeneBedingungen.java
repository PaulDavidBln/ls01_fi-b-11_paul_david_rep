import java.util.Scanner;
public class EigeneBedingungen {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		char sex;
		double koerpergroesse;
		double koerpergewicht;
		
		System.out.println("BMI Rechner");
		System.out.print("Bitte geben Sie ihr Geschlecht ein. Männlich = m, Weiblich = w: ");
		sex = myScanner.next().charAt(0);
		
		System.out.print("Bitte geben Sie ihre Körpergröße in m ein: ");
		koerpergroesse = myScanner.nextDouble();
		
		System.out.print("Bitte geben Sie ihr Körpergewicht in kg ein: ");
		koerpergewicht = myScanner.nextDouble();
		double bmi = koerpergewicht / (koerpergroesse * koerpergroesse); 
		
		if(sex == 'm') {
			if(bmi < 20) {
				System.out.println("Sie haben Untergewicht.");
			}else if(bmi <= 25){
				System.out.println("Sie haben Normalgewicht.");
			}else {
				System.out.println("Sie haben Übergewicht");
			}
		}else if(sex == 'w'){
			if(bmi < 19) {
				System.out.println("Sie haben Untergewicht.");
			}else if(bmi <= 24) {
				System.out.println("Sie haben Normalgewicht.");
			}else {
				System.out.println("Sie haben Übergewicht");
			}
			
		}else {
			System.out.println("Es ist ein Fehler aufgetreten.");
		}
		
		
	}

}
