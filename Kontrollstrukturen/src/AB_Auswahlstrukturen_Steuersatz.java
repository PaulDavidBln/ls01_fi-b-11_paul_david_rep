import java.util.Scanner;

public class AB_Auswahlstrukturen_Steuersatz {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie den Nettowert an: ");
		double netto = sc.nextDouble();
		System.out.print("Soll der ermäßigte Steuersatz genutzt werden? Bitte antworten sie mit j für ja oder mit n für nein. Andere Eingaben werden nicht akzeptiert: ");
		char benutzereingabe = sc.next().charAt(0);
		
		if(benutzereingabe == 'j') {
			double brutto = netto + (netto * 0.07); 
			System.out.println("Der Preis inkl. ermäßigtem Steuersatz beträgt " + brutto + " Euro");
		}else if(benutzereingabe == 'n'){
			double brutto = netto + (netto * 0.19);
			System.out.println("Der Preis inkl. dem vollen Steuersatz beträgt " + brutto + " Euro");
		}else {
			System.out.println("Es ist ein Fehler bei der Eingabe aufgetreten. Antworten sie biite mit \"j\" für ja oder mit \"n\" für nein.");
		}
		
	}
}
