import java.util.Scanner;
public class AB_Schleifen2_Million {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		zeitausrechnen();
	}
	
	public static void zeitausrechnen() {
		Scanner sc = new Scanner(System.in);
		System.out.print("Bitte geben Sie den jährlichen Zinswert ein: ");
		double zins = sc.nextDouble();
		System.out.print("Bitte geben Sie die einmalige Einlage an: ");
		double kapital = sc.nextDouble();
		double jahre = 0;
		
		while(kapital < 1000000) {
			kapital = (zins * kapital / 100) + kapital;
			jahre++;
		}
		System.out.println("Sie sind in " + (int)jahre + " Jahren Millionär.");
		
		System.out.println("\nMöchten Sie einen weiteren Programmdurchlauf durchführen? j für ja, n für nein");
		char weitererdurchlauf = sc.next().charAt(0);
		if(weitererdurchlauf == 'j') {
			zeitausrechnen();
		
		}
	}

}
