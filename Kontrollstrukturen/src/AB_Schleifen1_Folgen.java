
public class AB_Schleifen1_Folgen {

	public static void main(String[] args) {
		//Teil a
		for(int i = 99; i >=9; i-=3) {
			System.out.print(i + " ");
		}
		System.out.println("\n");
		
		//Teil b
		int a = 1;
		for(int i = 1; i <= 400; i+=a) {
			a+= 2;
			System.out.print(i + " ");
		}
		
		System.out.println("\n");
		
		//Teil c
		for(int i = 2; i <= 1; i+=4) {
			System.out.print(i + " ");
		}
		
		//Teil d
		a = 4;
		for(int i = 4; i <= 1024; i+=a) {
			a+=8;
			System.out.print(i + " ");
		}
	
		System.out.println("\n");
		
		//Teil e
		for(int i = 2; i <= 32768; i*=2) {
			System.out.print(i + " ");
		}
		
	}

}
