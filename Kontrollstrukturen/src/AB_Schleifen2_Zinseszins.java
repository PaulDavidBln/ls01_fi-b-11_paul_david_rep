import java.util.Scanner;
public class AB_Schleifen2_Zinseszins {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.print("Laufzeit (in Jahren) des Sparvertrags: ");
		double laufzeit = sc.nextDouble();
		System.out.print("Wie viel Kapital (in Euro) möchten Sie anlegen: ");
		double kapital = sc.nextDouble();
		System.out.print("jährlicher Zinssatz: ");
		double zins = sc.nextDouble();
		double jahre = 0;
		
		System.out.println("\nEingezahltes Kapital: " + kapital + " Euro");
		
		
		do {
			kapital = (zins * kapital / 100) + kapital;
			kapital = Math.round(kapital * 100)/100.00;
			jahre++;
		}
		while(laufzeit > jahre);
		
		
		System.out.println("Ausgezahltes Kapital: " + kapital + " Euro");
		sc.close();
	}

}
