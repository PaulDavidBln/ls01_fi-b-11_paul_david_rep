import java.util.Scanner;

public class MittelwertMitMethoden {

	public static void main(String[] args) {
		double anzahl;
		double zahl;
		double summe = 0;
		double m;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert den von Ihnen eingebenen Zahlen.");
		anzahl = eingabe(myScanner, "Bitte geben Sie die Anzahl der einzugebenden Zahlen ein: ");
		for(int x = 1; x <= anzahl; x++) {
			zahl = eingabe(myScanner, "Bitte geben Sie eine Zahl ein: ");
			summe +=zahl;
		}
		
		m = mittelwertBerechnung(anzahl, summe);
		
		ausgabe(m);
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double mittelwertBerechnung(double anzahl, double summe) {
		double m = summe / anzahl;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert der Zahlen ist: " + mittelwert);
	}
}
