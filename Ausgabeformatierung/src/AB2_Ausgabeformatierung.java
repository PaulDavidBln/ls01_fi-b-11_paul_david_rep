
public class AB2_Ausgabeformatierung {
	public static void main(String[] args) {
		System.out.println("Aufgabe 1\n");
		System.out.printf("%5s\n%s%7s\n%s%7s\n%5s", "**", "*", "*", "*", "*", "**");
		
		System.out.println("\n\nAufgabe 2");
		System.out.printf("%-5s%s%-19s%s%4s\n", "0!", "=", "", "=", "1");
		System.out.printf("%-5s%s%-19s%s%4s\n", "1!", "=", " 1", "=", "1");
		System.out.printf("%-5s%s%-19s%s%4s\n", "2!", "=", " 1 * 2", "=", "2");
		System.out.printf("%-5s%s%-19s%s%4s\n", "3!", "=", " 1 * 2 * 3", "=", "6");
		System.out.printf("%-5s%s%-19s%s%4s\n", "4!", "=", " 1 * 2 * 3 * 4", "=", "24");
		System.out.printf("%-5s%s%-19s%s%4s\n", "5!", "=", " 1 * 2 * 3 * 4 * 5", "=", "120");
		
		System.out.println("\nAufgabe 3");
		double z1 = -28.8889;
		double z2 = -23.3333;
		double z3 = -17.7778;
		double z4 = -6.6667;
		double z5 = 1.1111;
		// Ich weiß nicht, wie ich zwei Formatierungen für einen String(?) nutzen kann und habe deshalb mit Leerzeichen gearbeitet, das war auch der Grund weshalb ich keine Abgabe hatte
		System.out.printf("%-12s%s%10s\n", "Fahrenheit", "|", "Celsius");
		System.out.printf("-----------------------\n");
		System.out.printf("%-12s%s    %.2f\n", "-20", "|", z1);
		System.out.printf("%-12s%s    %.2f\n", "-10", "|", z2);
		System.out.printf("%-12s%s    %.2f\n", "0", "|", z3);
		System.out.printf("%-12s%s     %.2f\n", "+20", "|", z4);
		System.out.printf("%-12s%s      %.2f\n", "+30", "|", z5);
	}

}
