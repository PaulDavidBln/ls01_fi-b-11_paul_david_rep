
public class AB1_Ausgabeformatierung {

	public static void main(String[] args) {
		System.out.println("Aufgabe 1");
		System.out.printf("Das ist eine Ausgabe. ");
		System.out.print("Auch das ist eine Ausgabe.");
		System.out.printf("\n\"Oury Jalloh - das war Mord\" schrieten die Demonstranten. Hunderte Demonstranten gedenkten an Oury Jalloh in Dessau, dieser ist am 7. Januar 2005 in einer Polizeizelle verbrannt gefunden worden. Die Behörden verhindern bis heute die Aufklärung. \"Ein neues Gutachten kommt zu dem Schluss, dass der damals 36-Jährige mit hoher Wahrscheinlichkeit in der Zelle angezündet wurde.\" heißt es in einem Artikel der Tagesschau.\n\n");
		// Der Unterschied zwischen print() und println() liegt darin, dass println() einen Zeilenumbruch nach der Ausgabe einfügt.
		
		System.out.println("Aufgabe 2\n");
		System.out.printf("      *\n" + "     ***\n" + "    *****\n" + "   *******\n" + "  *********\n" + " ***********\n" + "*************\n" + "    ***\n" + "    ***\n");
		
		System.out.println("Aufgabe 3 ");
		double z1 = 22.4234234;
		double z2 = 111.2222;
		double z3 = 4.0;
		double z4 = 1000000.551; 
		double z5= 97.34;
		System.out.printf("%.2f\n%.2f\n%.2f\n%.2f\n%.2f\n", z1, z2, z3, z4, z5);
		
	}

}
