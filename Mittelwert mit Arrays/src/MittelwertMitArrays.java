import java.util.Scanner;

public class MittelwertMitArrays {
	public static void main(String[] args) {
		Scanner eingabe = new Scanner(System.in);
		System.out.print("Willkommen bei der Mittelwertberechnung. Bitte geben Sie ein, wie viele Zahlen Sie zur Berechnung nutzen möchten: ");
		int laenge = eingabe.nextInt();
		double summe = 0;
		double[] zahlen = new double[laenge];
		for(int x = 0; x < zahlen.length; x++) {
			System.out.print("Bitte geben Sie eine Zahl ein: ");
			zahlen[x] = eingabe.nextDouble();
		}
		for(int x = 0; x < zahlen.length; x++) {
			summe += zahlen[x];
		}
		double mittelwert = summe / zahlen.length;
		System.out.println("Der Mittelwert, der von Ihnen eingebenen Zahlen beträgt: " + mittelwert);
		eingabe.close();
	}

}
